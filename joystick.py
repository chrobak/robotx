#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Software License Agreement (BSD License)
#
#  Copyright (c) 2014, Ocean Systems Laboratory, Heriot-Watt University, UK.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of the Heriot-Watt University nor the names of
#     its contributors may be used to endorse or promote products
#     derived from this software without specific prior written
#     permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
#
#  Original authors:
#   Valerio De Carolis, Marian Andrecki, Corina Barbalata, Gordon Frost
#
#  Modified 2017, DROP Lab, university of Michigan, USA
#  Author:
#     Corina Barbalata, Eduardo Iscar, Atulya Shree

from __future__ import division

import traceback
import numpy as np
np.set_printoptions(precision=3, suppress=True)

import rospy
import roslib
import math


from sensor_msgs.msg import Joy
from vehicle_interface.msg import Vector6Stamped, PilotRequest
from vehicle_interface.srv import BooleanService
from utils.msg import ThrustersData
from std_msgs.msg import String, Int32


# config
TOPIC_JOY = 'joy'
TOPIC_CMD = 'thrusters/commands'
TOPIC_FORCES = 'user/forces'
TOPIC_STAY = 'sphere_a/pilot/body_req'
TOPIC_JOYON = '/user/joy'
SRV_THRUSTERS = 'sphere_a/thrusters/switch'
SRV_CONTROLLER = 'sphere_a/pilot/switch'
TOPIC_THR = 'sphere_hw_iface/thruster_data'
TOPIC_DIS_AXIS = 'sphere_a/user/dis_axis'
TOPIC_JOY_MODE = 'sphere_a/user/joy_mode'

JOYSTICK_RATE = 10 #Hz
# joystick sensitivity mapping
#   use 0.1 for a linear response
#   use 0.5 for a mild exponential response
#   use 1.0 for a heavy exponential response
K_EXP = 0.5

# thrust standard request
#   in default operation the joystick interface request only a fraction of maximum thrust
#   this is helping in small tank operations and it produces only 65% of full thrust
#   use the boost mode to squeeze all the juice out of the vehicle! :)
K_REDUCED = 0.95
MAX_THROTTLE = 0.5
MAX_THRUST = 7.86


AXIS = {
    'x': 1,     # left stick up/down
    'z': 3,     # right stick up/down
    'pitch': 4,   # left stick left/right
    'yaw': 0,    # right stick left/right
}

BTNS = {
    'sw_stay': 0,       # x
    'sw_controller': 1, # A
    'sw_thrusters': 7,  # B
    'sw_joy_complete': 3,       # y
    'depth': 2,         # RT
    'pitch': 6,         # LT
    'boost': 5,          # RB
    'start': 9          # start
}

MAX_U = np.array([16.00, 0.0, 16.00, 0.00, 10.0, 2.00])

MODE_FORCE = 'force'
MODE_SPEED = 'speed'
MODE_DIRECT = 'direct'


VEHICLE_AUV = 'auv'

#flags
FLAG_STAY = False
FLAG_COMPLETE_JOY_CTRL = True
FLAG_DEPTH = False
FLAG_AUTO = False


class JoystickInterface(object):

    def __init__(self, name, speed_limit, topic_input, topic_throttle, topic_stay, topic_forces, srv_thrusters, srv_controller, vehicle, k_exp=K_EXP):
        self.joystick_loop = rospy.Rate(JOYSTICK_RATE)

        self.name = name
        self.speed_limit = speed_limit
        self.input_topic = topic_input

        # joystick state
        self.last_seq = -1
        self.mode = MODE_FORCE
        self.depth_mode = 0
        self.boost_mode = 0
        self.vehicle = vehicle
        self.k_exp = np.clip(k_exp, 0.1, 2)     # clip exponential term (zero will produce errors)

        self.mode_controller = False
        self.mode_thrusters = False

        self.btn_switch = np.zeros(5)
        self.btn_thrusters = np.zeros(5)
        self.btn_controller = np.zeros(5)
        self.btn_stay = np.zeros(5)
        self.btn_joy_ctrl = np.zeros(5)
        self.btn_depth = np.zeros(5)

        self.joy_comms = np.zeros(6)
        self.joy_comms_prev = np.zeros(6)

        self.depth_mode = 0
        self.pitch_mode = 0

        self.flag_stay = FLAG_STAY
        self.flag_complete_joy_ctrl = FLAG_COMPLETE_JOY_CTRL
        self.flag_depth = FLAG_DEPTH
        self.flag_auto = FLAG_AUTO

        self.data_joy = np.zeros(6, dtype=np.float64)
        self.joy_mode = 0

        self.thr = np.zeros(4, dtype=np.float64)
        self.throttle = np.zeros(4, dtype=np.float64)
        self.forces_th = np.zeros(6, dtype=np.float64)

        self.dis_axis = np.zeros(6, dtype=np.float64)

        # limits on outputs
        self.thr_limit = MAX_THRUST * np.ones(4, dtype=np.float64)

        # ros interface

        #self.pub_thr = rospy.Publisher(topic_throttle, ThrusterCommand, tcp_nodelay=True, queue_size=1)
        self.pub_for = rospy.Publisher(TOPIC_FORCES, Vector6Stamped, tcp_nodelay=True, queue_size=1)
        self.pub_disable_axis = rospy.Publisher(TOPIC_DIS_AXIS, Vector6Stamped, tcp_nodelay=True, queue_size=1)
        self.pub_joy_mode = rospy.Publisher(TOPIC_JOY_MODE, Int32, tcp_nodelay=True, queue_size=1)
        self.pub_stay = rospy.Publisher(topic_stay, PilotRequest, tcp_nodelay=True, queue_size=1)
        self.pub_thr = rospy.Publisher(TOPIC_THR, ThrustersData, tcp_nodelay=True, queue_size=1)
        self.sub_joy = rospy.Subscriber(self.input_topic, Joy, self.handle_joystick, tcp_nodelay=True, queue_size=10)

        # services
        self.srv_thrusters = rospy.ServiceProxy(srv_thrusters, BooleanService)
        self.srv_controller = rospy.ServiceProxy(srv_controller, BooleanService)

        # rosinfo
        rospy.loginfo('%s started in mode: %s', self.name, self.mode)

    # def send_forces_thr(self):
    #     ns = ThrustersData()
    #     ns.ul = self.throttle[2]
    #     ns.ur = self.throttle[3]
    #     ns.fl = self.throttle[0]
    #     ns.fr = self.throttle[1]
    #
    #     self.pub_thr.publish(ns)

    def send_force(self):
        uf = Vector6Stamped()
        uf.header.stamp = rospy.Time.now()
        uf.values = self.forces_th.flatten().tolist()
        self.pub_for.publish(uf)

    def send_dis_axis(self):
        uf = Vector6Stamped()
        uf.header.stamp = rospy.Time.now()
        uf.values = self.dis_axis.flatten().tolist()
        self.pub_disable_axis.publish(uf)

    def send_joy_string(self):
        uf = Int32()
        uf.data = int(self.joy_mode)
        self.pub_joy_mode.publish(uf)


    # def load_alloc_matrix(self):
    #     # from cad model (old model)
    #     self.MT = np.array([
    #         [0, 0, 1, 1],
    #         [0, 0, 0, 0],
    #         [1, 1, 0, 0],
    #         [0.27878, -0.2802, -0.25172, -0.25172],
    #         [0.0, 0.0, 0.17057, -0.16823],
    #         [-0.0179, -0.0179, 0.02218, 0.002218]
    #     ])


    # def compute_thrust_throttle(self, forces):
    #
    #     self.load_alloc_matrix()  # load thruster allocation matrix
    #
    #     # mapp to thrusters
    #     self.MT_inv = np.linalg.pinv(self.MT)
    #     self.thr = np.dot(self.MT_inv, forces)
    #     self.thr = np.clip(self.thr, -self.thr_limit, self.thr_limit).astype(float)
    #
    #     #thrust to throttle direct mapping from graph
    #     # neutral position = 0.5
    #     # full forward = 1.0
    #     # full back = 0.0
    #     # MAX_THROTTLE = 0.5
    #     # MAX_THRUST = 7.86
    #
    #     self.throttle = 0.5 + self.thr*MAX_THROTTLE/MAX_THRUST
    #     self.throttle = np.clip(self.throttle, 0, 1).astype(float)

        # MAX_U = np.dot(self.MT, np.array([MAX_THRUST, MAX_THRUST, MAX_THRUST, MAX_THRUST]))

        # print 'Thrust', self.thr
        # print 'Throttle', self.throttle

    # def steering(self, x, y):
    #     # convert to polar
    #     r = math.hypot(x, y)
    #     t = math.atan2(y, x)
    #
    #     # rotate by 45 degrees
    #     t += math.pi / 4
    #
    #     # back to cartesian
    #     left = r * math.cos(t)
    #     right = r * math.sin(t)
    #
    #     # rescale the new coords
    #     left = left * math.sqrt(2)
    #     right = right * math.sqrt(2)
    #
    #     # clamp to -1/+1
    #     left = max(-1, min(left, 1))
    #     right = max(-1, min(right, 1))
    #
    #     return left, right

    def handle_joystick(self, data):
        # parse joystick
        surge = data.axes[AXIS['x']]            # normal joystick axis
        sway = 0 #-1 * data.axes[AXIS['y']]        # reverse joystick axis
        heave = -1 * data.axes[AXIS['z']]       # normal joystick axis


        roll = 0 #data.axes[AXIS['roll']]                                # not used
        pitch = 0 #data.axes[AXIS['pitch']]        # normal joystick axis
        yaw = -1 * data.axes[AXIS['yaw']]       # normal joystick axis


        # parse buttons
        #self.btn_switch[-1] = data.buttons[BTNS['sw_mode']]
        self.btn_thrusters[-1] = data.buttons[BTNS['sw_thrusters']]
        self.btn_controller[-1] = data.buttons[BTNS['sw_controller']]
        self.btn_stay[-1] = data.buttons[BTNS['sw_stay']]
        self.btn_joy_ctrl[-1] = data.buttons[BTNS['sw_joy_complete']]
        self.btn_depth[-1] = data.buttons[BTNS['depth']]

        self.roll_mode = data.buttons[BTNS['pitch']]
        self.boost_mode = data.buttons[BTNS['boost']]

        if self.btn_joy_ctrl[-1] == 1 and self.btn_joy_ctrl[-2] == 0:  #total joy control
            self.flag_complete_joy_ctrl = True
            self.flag_depth = False
            self.flag_stay = False
            self.flag_auto = False

        if self.btn_stay[-1] == 1 and self.btn_stay[-2] == 0:  #station keeping
            try:
                pr = PilotRequest()
                pr.header.stamp = rospy.Time.now()
                pr.priority = PilotRequest.HIGH
                pr.position = np.zeros(6)
                self.pub_stay.publish(pr)
                self.data_joy = np.array([0, 0, 0, 0, 0, 0])
            except Exception:
                rospy.logerr('%s: publish error', self.name)
            self.flag_stay = True
            self.flag_depth = False
            self.flag_complete_joy_ctrl = False
            self.flag_auto = False

        if self.btn_depth[-1] == 1 and self.btn_depth[-2] == 0: # autonomous depth. joystick the rest
            self.dis_axis = np.array([0, 0, 1, 0, 0, 0])
            try:
                pr = PilotRequest()
                pr.header.stamp = rospy.Time.now()
                pr.priority = PilotRequest.HIGH
                pr.position = np.zeros(6)
                self.pub_stay.publish(pr)
                self.data_joy[2] = 0
            except Exception:
                rospy.logerr('%s: publish error', self.name)
            self.flag_depth = True
            self.flag_stay = False
            self.flag_complete_joy_ctrl = False
            self.flag_auto = False

        if self.btn_controller[-1] == 1 and self.btn_controller[-2] == 0: # turn off joystick
            self.flag_auto = True
            self.flag_depth = False
            self.flag_stay = False
            self.flag_complete_joy_ctrl = False


        if self.flag_complete_joy_ctrl == True:
            self.data_joy = np.array([surge, sway, heave, roll, pitch, yaw])
            self.mode_controller = False
            response = self.srv_controller.call(self.mode_controller)
            rospy.loginfo('Controller disabled, full joystick control %s',  response)
            self.joy_mode = 0 #"teleoperated mode"

        if self.flag_depth == True:
            self.data_joy = np.array([surge, sway, 0.0, roll, pitch, yaw])
            self.mode_controller = False
            rospy.loginfo('autonomous depth keeping')
            response = self.srv_controller.call(self.mode_controller)
            rospy.loginfo('Controller enabled, autonomous depth keeping %s',  response)
            self.joy_mode = 1 #"depth keeping"

        if self.flag_stay == True:
            self.mode_controller = False
            response = self.srv_controller.call(self.mode_controller)
            rospy.loginfo('Controller enabled, station keeping %s', response)
            self.joy_mode = 2 #"station keeping"


        if self.flag_auto == True:
            self.mode_controller = True
            response = self.srv_controller.call(self.mode_controller)
            rospy.loginfo(' Controller enabled, autonomous behaviour: %s', response)
            self.data_joy = np.array([0, 0, 0, 0, 0, 0])
            self.joy_mode = 3 #"autonomous mode"




        self.btn_stay = np.roll(self.btn_stay, -1)
        self.btn_joy_ctrl = np.roll(self.btn_joy_ctrl, -1)
        self.btn_depth = np.roll(self.btn_depth, -1)
        self.joy_comms_prev = self.joy_comms




    def mapping_joystick(self):

        if self.boost_mode == 1:
            k_scaling = K_REDUCED               # request full thrust
        else:
            if self.data_joy[0] > 0:
                k_scaling = np.array([1, 1, 1, 1, 1, 1-self.data_joy[0]])
            else:
                k_scaling = np.array([1, 1, 1, 1, 1, 1 + self.data_joy[0]])
            #k_scaling = K_REDUCED       # request only a fraction of max thrust

        # process joystick input
        forces = k_scaling * self.data_joy
        # exponential mapping
        forces = np.sign(forces) * (np.exp(self.k_exp * np.abs(forces)) - 1) / (np.exp(self.k_exp) - 1)
        forces = np.clip(forces, -1, 1)

        # forces clipping
        self.forces_th = forces*MAX_U

        # FORCES command mode default
        self.send_force()
        self.send_dis_axis()
        self.send_joy_string()

        # self.compute_thrust_throttle(self.forces_th)
        # self.send_forces_thr()

    def run(self):
        # init pilot
        rospy.loginfo('%s: joystick initialized ...', self.name)

        # pilot loop
        while not rospy.is_shutdown():

            # run main pilot code
            self.mapping_joystick()

            try:
                self.joystick_loop.sleep()
            except rospy.ROSInterruptException:
                self.mode_controller = True





if __name__ == '__main__':
    rospy.init_node('node_joystick')
    rospy.loginfo('%s initializing ...', rospy.get_name())

    # load parameters
    #throttle_limit = int(rospy.get_param('thrusters/throttle_limit', tc.MAX_THROTTLE))
    throttle_limit = 85
    topic_input = rospy.get_param('~input_topic', TOPIC_JOY)
    topic_throttle = rospy.get_param('~topic_throttle', TOPIC_CMD)
    topic_stay = rospy.get_param('~topic_stay', TOPIC_STAY)
    topic_forces = rospy.get_param('~topic_forces', TOPIC_FORCES)
    srv_thrusters = rospy.get_param('~srv_thrusters', SRV_THRUSTERS)
    srv_controller = rospy.get_param('~srv_controller', SRV_CONTROLLER)
    vehicle = rospy.get_param('~vehicle', VEHICLE_AUV)
   # wait_services = bool(rospy.get_param('~wait_services', False))

    # check valid limit
    throttle_limit = np.clip(throttle_limit, -100, 100)

    # console output
    rospy.loginfo('%s throttle limit: %d%%', rospy.get_name(), throttle_limit)
    rospy.loginfo('%s topic input: %s', rospy.get_name(), topic_input)
    rospy.loginfo('%s topic throttle: %s', rospy.get_name(), topic_throttle)
    rospy.loginfo('%s topic stay: %s', rospy.get_name(), topic_stay)
    rospy.loginfo('%s topic forces: %s', rospy.get_name(), topic_forces)
    rospy.loginfo('%s service thrusters: %s', rospy.get_name(), srv_thrusters)
    rospy.loginfo('%s service controller: %s', rospy.get_name(), srv_controller)
    rospy.loginfo('%s vehicle: %s', rospy.get_name(), vehicle)
   # rospy.loginfo('%s wait services: %s', rospy.get_name(), wait_services)


    js = JoystickInterface(rospy.get_name(), throttle_limit, topic_input, topic_throttle, topic_stay, topic_forces,
                           srv_thrusters, srv_controller, vehicle)

    try:
        js.run()
    except Exception:
        tb = traceback.format_exc()
        rospy.logfatal('%s uncaught exception, dying!\n%s', 'node_joystick', tb)

        rospy.spin()